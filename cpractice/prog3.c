#include <stdio.h>

/* 
question 1 answer = B 
A: b->var; incorrect, b is a struct not a pointer to a struct
B: b.var; correct, we want struct b, variable 'var'
C: b-var; incorrect, dont want struct b-var w/ no declared variable 
D: b>var; incorrect 'b is greater than var'

question 2 answer = A 
A: b->var; correct, b is a pointer to a struct & we want variable 'var'
B: b.var; incorrect, b is a pointer
C: b-var; incorrect, we dont want struct b-var w/ no declared variable 
D: b>var; incorrect 'b is greater than var'

question 3 answer = D 
A: doesnt list struct name
B: forgets semi colon after { }
C: does not encase the contents of the struct in a { }
D: is the only choice that correctly ends with semi colon

question 4 answer = B 
'struct' then 'name of struct' then 'variable name':
A: struct foo -- incorrect, missing variable name after foo
B: struct foo var -- correct syntax
C: foo -- incorrect, not affecting the struct
D: int foo -- incorrect, not affecting struct either
*/
	
struct b{
	int var;
};

int main(){
	printf("prog3.c:\n");

	struct b example;
	struct b *ptrExample;

	ptrExample = &example;

	example.var = 666;
	printf("%d\n", example.var);

	ptrExample-> var = 777;
	printf("%d\n", example.var);

	return 0;
}
