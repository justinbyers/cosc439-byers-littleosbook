#include <stdio.h>
int main(){
	printf("prog1.c:\n");

	int number = 12.34;
	printf("the following use int number = 12.34:\n");
	printf("value of number (dec) : %d \n", number);
	printf("value of number (float) : %f \n", number);
	printf("value of number (sci-not) : %e \n", number);
	printf("value of number (uns-int) : %u \n", number);
	printf("value of number (octal) : %o \n", number);
	printf("value of number (hex) : %x \n", number);
	
	double d_number = 12.34;
	printf("int variables do not correctly display float and scientific notation\ntherefore the following use double d_number = 12.34:\n");
	printf("value of d_number (float) : %f \n", d_number);
	printf("value of d_number (sci-not) : %e \n", d_number);


	return 0;
}
