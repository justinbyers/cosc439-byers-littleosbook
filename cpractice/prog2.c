#include <stdio.h>

/* question 1 answer = D 
A: int x; just an int
B: int &x; invalid, would store memory addr
C: ptr x; invalid declaration 
D: int *x;  correct pointer 

question 2 answer = C
A: *a -- incorrect, would print the value of what 'a' points to
B: a -- incorrect, prints value of 'a'
C: &a -- correct, memory addr of int variable 'a'
D: address(a) -- incorrect, not sure this is ever valid?

question 3 answer = C 
A: a; -- incorrect; just value of a
B: val(a); -- incorrect
C: *a; -- correct, we want val stored at address pointed to by a
D: &a; -- incorrect, we dont want address of a
*/

int main(){
	printf("prog2.c:\n");

	int *d; // question 1

	int i = 0;
	int* iptr;
	iptr = NULL;
	printf("iptr = %x\n", iptr);
	iptr = &i; // changes addr from null -- proves question 2
	printf("iptr = %x\n", iptr);

	i = 123; 
	printf("i = %d\n", i);
	printf("iptr = %d\n", *iptr);

	*iptr = 321;
	printf("i = %d\n", i);
	printf("iptr = %d\n", *iptr); //gives same answer as i -- proves Q3

	return 0;
}
